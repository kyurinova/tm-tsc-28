package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void addAll(@NotNull List<E> list);

    void remove(final E entity);

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@Nullable Comparator<E> comparator);

    void clear();

    @Nullable
    E findById(@NotNull String id);

    @NotNull
    E findByIndex(@NotNull Integer index);

    @Nullable
    E removeById(@NotNull String id);

    @Nullable
    E removeByIndex(@NotNull Integer index);

    boolean existsById(@NotNull String id);

    boolean existsByIndex(@NotNull Integer index);

}
