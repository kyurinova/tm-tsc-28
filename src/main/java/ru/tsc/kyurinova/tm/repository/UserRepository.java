package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        return entities.stream()
                .filter(u -> login.equals(u.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull String email) {
        return entities.stream()
                .filter(u -> email.equals(u.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeById(@NotNull String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        entities.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user);
        return user;
    }
}
