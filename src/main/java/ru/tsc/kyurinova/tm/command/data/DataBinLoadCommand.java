package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractDataCommand;
import ru.tsc.kyurinova.tm.dto.Domain;
import ru.tsc.kyurinova.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinLoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-load-bin";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Lod binary data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
